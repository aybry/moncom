up:
	docker-compose up -d

build:
	docker-compose up -d --build

stop:
	docker-compose stop

down:
	docker-compose down

status:
	watch docker-compose ps

logs:
	docker-compose logs --tail=100 -f
