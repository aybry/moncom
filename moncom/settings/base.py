import os
import logging

from pathlib import Path


BASE_DIR = Path(__file__).resolve().parent.parent.parent


LOGIN_REDIRECT_URL = "home"


SECRET_KEY = os.getenv("SECRET_KEY")


INSTALLED_APPS = [
    "supervision",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "channels",
    "rest_framework",
    "captcha",
    "versatileimagefield",
]


MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]


ROOT_URLCONF = "moncom.urls"


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]


WSGI_APPLICATION = "moncom.wsgi.application"
ASGI_APPLICATION = "moncom.asgi.application"


AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True


STATIC_URL = "/staticfiles/"
STATIC_ROOT = BASE_DIR / "staticfiles"
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"


REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.BasicAuthentication",
        "rest_framework.authentication.SessionAuthentication",
    ),
}


REDIS_HOSTNAME = "redis"


EMAIL_PORT = 25
EMAIL_HOST_USER = os.getenv("PM_MONCOM_USERNAME")
DEFAULT_FROM_EMAIL = os.getenv("PM_MONCOM_USERNAME")
SERVER_EMAIL = os.getenv("PM_MONCOM_USERNAME")
EMAIL_HOST_PASSWORD = os.getenv("PM_MONCOM_PASSWORD")
EMAIL_USE_TLS = True

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_FILE_PATH = BASE_DIR / "apps" / "emails"

MEDIA_URL = "/media/"
MEDIA_ROOT = BASE_DIR / "media"

RECAPTCHA_PUBLIC_KEY = os.getenv("GOOGLE_RECAPTCHA_PUBLIC_KEY")
RECAPTCHA_PRIVATE_KEY = os.getenv("GOOGLE_RECAPTCHA_PRIVATE_KEY")


def init_logger():
    Path("logs").mkdir(exist_ok=True)

    logger = logging.getLogger("moncom")
    logger.setLevel(logging.DEBUG)
    fh_debug = logging.FileHandler("logs/debug.log")
    fh_debug.setLevel(logging.DEBUG)
    fh_info = logging.FileHandler("logs/info.log")
    fh_info.setLevel(logging.INFO)
    fh_warning = logging.FileHandler("logs/warning.log")
    fh_warning.setLevel(logging.WARNING)

    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    fh_debug.setFormatter(formatter)
    fh_info.setFormatter(formatter)
    fh_warning.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh_debug)
    logger.addHandler(fh_info)
    logger.addHandler(fh_warning)
    logger.addHandler(ch)

    return logger


LOGGER = init_logger()
