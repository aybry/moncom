from .base import *


PROTONMAIL_HOST = "172.17.0.2"
DEBUG = True


ALLOWED_HOSTS = [
    "moncom-dev.ay-bryson.com",
    "0.0.0.0",
]


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("POSTGRESQL_DBNAME"),
        "USER": os.getenv("POSTGRESQL_USERNAME"),
        "PASSWORD": os.getenv("POSTGRESQL_PASSWORD"),
        "HOST": os.getenv("POSTGRESQL_HOST"),
        "PORT": os.getenv("POSTGRESQL_DBPORT"),
    }
}


CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(REDIS_HOSTNAME, 6379)],
        },
    },
}


EMAIL_HOST = PROTONMAIL_HOST
