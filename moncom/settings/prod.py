from .base import *


PROTONMAIL_HOST = "protonmail-bridge"
CELERY_BROKER_URL = f"redis://{REDIS_HOSTNAME}:6379"
CELERY_RESULT_BACKEND = f"redis://{REDIS_HOSTNAME}:6379"
DEBUG = False
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")


ALLOWED_HOSTS = [
    "moncom.ay-bryson.com",
    "159.89.9.147",
]


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("POSTGRESQL_DBNAME"),
        "USER": os.getenv("POSTGRESQL_USERNAME"),
        "PASSWORD": os.getenv("POSTGRESQL_PASSWORD"),
        "HOST": os.getenv("POSTGRESQL_HOST"),
        "PORT": os.getenv("POSTGRESQL_DBPORT"),
    }
}


CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(REDIS_HOSTNAME, 6379)],
        },
    },
}

EMAIL_HOST = PROTONMAIL_HOST
