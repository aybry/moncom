import os
import django

from django.urls import path
from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "moncom.settings")
django.setup()
django_asgi_app = get_asgi_application()

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

from supervision.consumers.session_consumers import ControllerConsumer, ReceiverConsumer
from supervision.consumers.template_consumer import TemplateConsumer


application = ProtocolTypeRouter(
    {
        "http": django_asgi_app,
        "websocket": AuthMiddlewareStack(
            URLRouter(
                [
                    path("ws/controller/<session_id>", ControllerConsumer.as_asgi()),
                    path("ws/receiver/<session_id>", ReceiverConsumer.as_asgi()),
                    path("ws/template/<template_id>", TemplateConsumer.as_asgi()),
                ]
            )
        ),
    }
)
