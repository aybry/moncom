
class MessageRow {
    constructor(data) {
        this.content = data.content;
        this.content_stripped = data.content_stripped;
        this.datetime = new Date(Date.parse(data.timestamp));
        this.time = this.datetime.toISOString().slice(-13, -5);
        this.date = formatDate(this.datetime);
        this.duration = data.duration;
        this.type = data.type;
    }

    get jq() {
        var $tr = $("<tr class='message-table-row'></tr>");

        var $type = $(`<td class="cell-with-icon"><i class='fa-large far fa-comment'></i></td>`)
        // var $date = $(`<td></td>`).text(this.date);
        // var $time = $(`<td></td>`).text(this.time);
        var $content = $(`<td class="text-column"></td>`)
            .text(this.content_stripped)
            .attr("title", this.content);

        $tr.append($type)
        // .append($date)
        // .append($time)
        .append($content);
        return $tr;
    }

    create() {
        $("#message-table > tbody").prepend(this.jq);
    }
}


class Participant {
    constructor(data) {
        this.id = data["id"];
        // this.ipAddress = data["ip_address"];
        this.name = data["name"];
        this.permitted = data["is_permitted"];
        this.joinedAt = data["joined_at"];
        // this.lastSeen = data["last_seen"];

        return this.jq;
    }

    get jq() {
        var $container = $(`<div class="participant" id="${this.id}"></div>`);
        var $name = $(`<div class="participant-name"></div>`).html(this.name ? this.name : "<i>[unnamed]</i>");
        var $permissionWidget = new PermissionWidget(this.permitted, this).jq;
        return $container.append($name).append($permissionWidget);
    }

    // appendToList() {
    //     print("Appending");
    //     $("#participants-list").append(this.jq);
    // }
}


class PermissionWidget {
    constructor(permitted, participant) {
        this.permitted = permitted;
        this.participant = participant;
    }

    get jq() {
        var icon = this.permitted ? "eye" : "eye-slash";
        var permitTitle = this.permitted ? "User can receive messages" : "User cannot receive messages";
        var $icon = $(`<i class="far fa-${icon}" title="${permitTitle}"></i>`);
        var $container = $(`<div class="participant-permission"></div>`).html($icon);
        $container.on("click", () => {
            toggleParticipantPermission(this.participant);
        })
        return $container;
    }
}


function updateParticipant(data) {
    var newParticipant = new Participant(data);
    $(`#${data.id}`).prepend(newParticipant.jq).remove();
}


function makeParticipantsList(participants) {
    $("#participants-list").html("");
    participants.forEach(ptcp_data => {
        var ptcp = new Participant(ptcp_data);
        $("#participants-list").append(ptcp);
    })
}


function copyToClipboard() {
    $tempInput = $("<input>");
    $tempInput.val($("#url-link").text()).select();
    navigator.clipboard.writeText($tempInput.val())
}


function makeLinks(sessionID) {
    var receiverURL = getURL('receiver', sessionID);

    $("#iframe-container").html(`<iframe src="/receiver/${sessionID}"></iframe>`);
    $("#url-session-id").text(sessionID);

    // var $iconContainer = $("<p></p>");

    // $iconContainer.append(
    //     $("<button><i class='fa-large symbol-clickable far fa-clone' title='Copy to clipboard'></i></button>").on("click", function() {
    //         copyToClipboard();
    //     })
    // ).append(
    //     $("<button><i class='fa-large symbol-clickable fas fa-external-link-alt' title='Open receiver link'></i></button>").on("click", function() {
    //         openReceiver(sessionID);
    //     })
    // )

    // $("#url-link-container").html(
    //     $(`<span id="url-link">${receiverURL}</span>`)
    // ).append($iconContainer);

    $("#copy-link-button").attr("data-url-link", receiverURL);
    $("#copy-link-button > span").text(receiverURL)
    makeQRCode(receiverURL);
}


$(document).ready(function() {
    const sessionID = JSON.parse($("#session-id").text());
    const receiverURL = getURL('receiver', sessionID);

    startSocket();

    $("#show-qr-code-button").on("click", () => {
        showPopup("qr-code-container");
    })

    $("#copy-link-button").on("click", () => {
        copyToClipboard();
    }).on("hover", (e) => {
        $("#copy-link-button > span").html("Copy link")
    }).on("unhover", (e) => {
        $("#copy-link-button > span").html(receiverURL)
    })

    $("#open-receiver-button").on("click", () => {
        openReceiver(sessionID);
    })

    initTinyTextbox("#input-textarea");
})