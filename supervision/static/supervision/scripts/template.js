
class PredefTextRow {
    constructor(data) {
        this.content = data.content;
        this.content_stripped = data.content_stripped;
        this.duration = data.duration;
        this.type = data.type;
    }

    get jq() {
        var $div = $(`<div class="predef-text-item"></div>`);
        var $timer = $(`<div class="predef-text-item-timer"><i class="fas fa-stopwatch"></i>${this.duration}</div>`);
        var $content = $(`<div class="predef-text-item-content">${this.content_stripped}</div>`);
        $div.append($content).append($timer);
        return $div;
    }

    get preview() {
        return $(`<div class="message-preview">${this.content}</div>`);
    }

    create() {
        var $elem = this.jq;
        this.elem = $elem;
        $("#text-list").prepend($elem);
        var row = this;
        $elem.hover(() => {
            showPreview(row.content);
        }, () => {
            clearPreview()
        });

    }
}


function showPreview(rowContent) {
    $("#text-here").html(rowContent);
}


function clearPreview() {
    $("#text-here").html("");
}


$(document).ready(function() {
    startSocket();

    $("#text-tab").click();

    initTinyTextbox("#input-textarea");

    $("#image-upload").on("dragenter")
})
