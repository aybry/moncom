
function print(input) {
    console.log(input);
}


function showPopup(popupID) {
    $("#popup-container > div").hide();
    $("#popup-container").show();
    $(`#${ popupID }`).show();
}


function getURL(page, arg) {
    return `${window.location.origin}/${page}/${arg}`
}


function formatDate(dateObj) {
    var elems = dateObj.toLocaleString("de-DE").split(",")[0].split(".");
    elems[0] = ("0" + elems[0]).slice(-2);
    elems[1] = ("0" + elems[1]).slice(-2);
    return elems.join(".");
}


function makeQRCode(sessionURL) {
    var qrURLEncoded = encodeURIComponent(sessionURL);

    $("#qr-code-img").attr("src", `https://api.qrserver.com/v1/create-qr-code/?data=${qrURLEncoded}&amp;size=200x200&bgcolor=196-196-196&color=31-31-31`);
}


function openController(sessionID) {
    window.open(getURL("controller", sessionID), "_self");
}


function openReceiver(sessionID) {
    window.open(getURL("receiver", sessionID));
}


function openTemplate(templateID) {
    window.open(getURL("templates", templateID), "_self");
}


function initTinyTextbox(selector) {
    tinymce.init({
        selector: selector,
        menubar: false,
        toolbar: "fontsizeselect bold italic underline alignleft aligncenter alignright bullist numlist code forecolor backcolor",
        width: "100%",
        height: "100%",
        resize: false,
        // placeholder: "Enter to send",
        setup: function(editor) {
            editor.on("init", function (e) {
                editor.execCommand("justifyCenter", true);
            });

            editor.on("keydown", function(e) {
                if (!e.shiftKey && e.keyCode == 13) {
                    var currentMessageHTML = this.getContent();
                    // Send new message to group if enter pressed (and not shift)
                    sendTextMessage(currentMessageHTML);
                    e.preventDefault();
                    editor.setContent("");
                    editor.execCommand("justifyCenter", true);
                }
            });

            editor.on("keyup", function(e) {
                if (window.location.pathname.search("templates") != -1) {
                    var currentMessageHTML = this.getContent();
                    // Show preview for any other key if on template editor
                    showPreview(currentMessageHTML);
                }
            })
        }
    })
}


$(document).ready(function() {
    $("#popup-container").on("click", function(event) {
        if (event.target == $("#popup-container")[0]) {
            $("#popup-container").hide();
        }
    })

    $("#menu").hover(function(event) {
        $(".label-container").addClass("open");
    }, function() {
        $(".label-container").removeClass("open");
    })
})
