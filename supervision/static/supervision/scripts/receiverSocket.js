function startSocket() {
    const sessionID = JSON.parse($("#session-id").text());
    const socketType = window.location.protocol === "https:" ? "wss" : "ws";
    const portNumber = socketType === "wss" ? ":7999" : ":8000";

    var wsURL = socketType + "://"
        + window.location.hostname
        + portNumber
        + "/ws/receiver/"
        + sessionID;

    recvSocket = new WebSocket(wsURL);

    recvSocket.onopen = function() {
        $("#socket-status").parent().children("span").text("Connected");
        $("#socket-status")
            .removeClass("disconnected")
            .addClass("connected");
    }

    recvSocket.onclose = function() {
        print("Socket disconnected")
        recvSocket = null
        $("#socket-status").parent().children("span").text("Disconnected");
        $("#socket-status")
            .removeClass("connected")
            .addClass("disconnected");
        console.error('Socket closed unexpectedly. Retrying...');

        setTimeout(startSocket, 1000);
    }

    recvSocket.onmessage = function(event) {
        var data = JSON.parse(event.data);
        print(data)
        if (data["method"] === "new_text_message") {
            $("#image-here").hide();
            $("#text-here").html(data.message.content);
            $("#text-here").show();
        } else if (data["method"] === "new_image") {
            $("#text-here").hide();
            $("#image-here").src(data["message"]);
            $("#image-here").show();
        }
    };
}


function submitParticipantDetails() {
    var name = $("#participant-name").val();
    var password = $("#session-password").val();

    recvSocket.send(JSON.stringify({
        "method": "submit_participant_details",
        "name": name,
        "password": password,
    }))

    $("#popup-container").hide();
}


$(document).ready(function() {
    startSocket();
})
