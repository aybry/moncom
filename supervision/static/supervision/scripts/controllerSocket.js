function sendTextMessage(messageContent) {
    socket.send(JSON.stringify({
        "method": "new_text_message",
        "message": messageContent,
        // TODO: duration
        "duration": 10,
    }))
}


function setSessionStatus(newStatus) {
    socket.send(JSON.stringify({
        "method": "set_status",
        "status": newStatus,
    }))
    if (newStatus === "PWD") {
        $("#session-password-container").show();
    } else {
        $("#session-password-container").hide();
    }
    $("[data-radio-group='session-status']").removeClass("active");
    $(`[data-radio-value='${newStatus}']`).addClass("active");
}


function activateSessionPasswordInput() {
    $("#session-password-display-wrapper").hide();
    $("#session-password-input-wrapper").show();
}


function setSessionPassword() {
    var password = $("#session-password-input").val();
    socket.send(JSON.stringify({
        "method": "set_password",
        "password": password,
    }))
    $("#session-password-input-wrapper").hide();
    $("#session-password-display").html(password);
    $("#session-password-display-wrapper").show();
}


function toggleParticipantPermission(participant) {
    socket.send(JSON.stringify({
        "method": "set_participant_permission",
        "participant_id": participant.id,
        "permitted": !(participant.permitted),
    }))
}


function startSocket() {
    const sessionID = JSON.parse($("#session-id").text());
    const socketType = window.location.protocol === "https:" ? "wss" : "ws";
    const portNumber = socketType === "wss" ? ":7999" : ":8000";

    makeLinks(sessionID);

    var wsURL = socketType + "://"
        + window.location.hostname
        + portNumber
        + "/ws/controller/"
        + sessionID;

    socket = new WebSocket(wsURL);

    socket.onopen = function() {
        // $("#socket-status").parent().children("span").text("Connected");
        $("#socket-status")
            .removeClass("disconnected fa-unlink")
            .addClass("connected fa-link");
    }

    socket.onclose = function() {
        socket = null
        // $("#socket-status").parent().children("span").text("Disconnected");
        $("#socket-status")
            .removeClass("connected fa-link")
            .addClass("disconnected fa-unlink");
        console.error('Socket closed unexpectedly. Retrying...');

        print('Trying to connect...')
        setTimeout(startSocket, 1000);
    }

    socket.onmessage = function(event) {
        var data = JSON.parse(event.data);
        print(data)

        if (data["method"] === "new_text_message") {
            var msg = new MessageRow(data.message);
            msg.create();
        } else if (data["method"] === "list_participants") {
            makeParticipantsList(data.message);
        } else if (data["method"] === "toggle_permission") {
            updateParticipant(data.message);
        }
    };
}
