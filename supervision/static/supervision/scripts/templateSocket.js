function sendTextMessage(messageContent) {
    socket.send(JSON.stringify({
        method: "new_predef_message",
        content: messageContent,
        // TODO: duration
        duration: 0,
    }))
}


function fetchMessages() {
    socket.send(JSON.stringify({
        method: "fetch_predef_messages",
    }))
}


function startSocket() {
    const templateID = JSON.parse($("#template-id").text());
    const socketType = window.location.protocol === "https:" ? "wss" : "ws";
    const portNumber = socketType === "wss" ? ":7999" : ":8000";

    var wsURL = socketType + "://"
        + window.location.hostname
        + portNumber
        + "/ws/template/"
        + templateID;

    socket = new WebSocket(wsURL);

    socket.onopen = function() {
        print("Connected");
        $("#socket-status").parent().attr("title", "Socket is connected");
        $("#socket-status")
            .removeClass("disconnected fa-unlink")
            .addClass("connected fa-link");

        fetchMessages();
    }

    socket.onclose = function() {
        socket = null
        $("#socket-status").parent().attr("title", "Socket is disconnected");
        $("#socket-status")
            .removeClass("connected fa-link")
            .addClass("disconnected fa-unlink");
        console.error('Socket closed unexpectedly. Retrying...');

        print('Trying to connect...')
        setTimeout(startSocket, 1000);
    }

    socket.onmessage = function(event) {
        var data = JSON.parse(event.data);
        print(data)

        if (data["method"] === "fetch_predef_messages") {
            $("#text-list").html("");
            data["messages"].forEach(msg => {
                var msgRow = new PredefTextRow(msg);
                msgRow.create();
            })
        } else if (data["method"] === "new_predef_text") {
            var msgRow = new PredefTextRow(data["message"]);
            msgRow.create();
        }
    };
}


function openTab(tabID) {
    $(".template-page").hide()
    $(".template-tab").removeClass("active");

    $(`#${ tabID }-page`).show()
    $(`#${ tabID }-tab`).addClass("active");
}

