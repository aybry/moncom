from django.db.models import base
from rest_framework.routers import DefaultRouter

from .views import PredefTextViewSet


router = DefaultRouter()
router.register('predef_texts', PredefTextViewSet, basename='predef_texts')

