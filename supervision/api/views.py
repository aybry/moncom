# from rest_framework.mixins import (
#     CreateModelMixin, ListModelMixin, RetrieveModelMixin, UpdateModelMixin
# )
from re import template
from rest_framework.viewsets import ModelViewSet

from ..models import PredefText, PredefImage, Template
from .serialisers import PredefTextSerialiser, PredefImageSerialiser


class PredefTextViewSet(ModelViewSet):
    serializer_class = PredefTextSerialiser
    # queryset = PredefText.objects.all()

    def get_queryset(self):
        set_id = self.request.query_params.get('set_id')

        try:
            template_set = Template.objects.get(id=set_id)
            print(template_set)
        except Template.DoesNotExist:
            return PredefText.objects.none()

        print(PredefText.objects.filter(set=template_set))
        return PredefText.objects.filter(set=template_set)


    #   def get(self, request, format=None):
    #       content =

    # def list(self, request):
    #     print(request.POST)

# PredefImage