from rest_framework import serializers

# from rest_flex_fields import FlexFieldsModelSerializer
# from versatileimagefield.serializers import VersatileImageFieldSerializer

from .. import models


class ParticipantSerialiser(serializers.ModelSerializer):
    class Meta:
        model = models.Participant
        fields = (
            "id",
            "is_permitted",
            "name",
            "ip_address",
            "joined_at",
        )


class MessageSerialiser(serializers.ModelSerializer):
    session_id = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()
    content_stripped = serializers.SerializerMethodField()
    duration = serializers.SerializerMethodField()

    def get_session_id(self, obj):
        return obj.session.id

    def get_type(self, obj):
        return obj.get_type()

    def get_content(self, obj):
        return obj.get_child().content

    def get_content_stripped(self, obj):
        return obj.get_child().strip_html()

    def get_duration(self, obj):
        return obj.get_child().duration

    def create(self, validated_data):
        return models.Message(**validated_data)

    class Meta:
        model = models.Message
        fields = (
            "session_id",
            "timestamp",
            "duration",
            "content_stripped",
            "type",
            "content",
        )


class PredefTextSerialiser(serializers.ModelSerializer):
    template = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()
    content_stripped = serializers.SerializerMethodField()

    def get_template(self, obj):
        return str(obj.template)

    def get_type(self, obj):
        return "text"

    def get_content(self, obj):
        return obj.content

    def get_content_stripped(self, obj):
        return obj.strip_html()

    def get_duration(self, obj):
        return obj.duration

    def create(self, validated_data):
        return models.PredefText(**validated_data)

    class Meta:
        model = models.PredefText
        fields = (
            "template",
            "duration",
            "content",
            "content_stripped",
            "type",
        )


# class ImageSerializer(FlexFieldsModelSerializer):
#     content = VersatileImageFieldSerializer(
#         sizes = (
#             ('full_size', 'url'),
#             ('thumbnail', 'thumbnail__100x100'),
#         )
#     )

#     class Meta:
#         model = models.PredefImage
#         fields = (
#             "name",
#             "template",
#             "content",
#             "image_ppoi",
#             "duration",
#             "id",
#         )
