import json
import logging

from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async as ds2a
from asgiref.sync import sync_to_async
from django.contrib.auth.models import User

from .. import models
from ..api import serialisers


LOGGER = logging.getLogger("moncom")


def set_status(session: models.Session, status: str):
    session.status = status
    session.save()
    LOGGER.debug(f"Set status of session {session.id} to {status}")


def make_participant(ip_address: str, session: models.Session, name=""):
    return models.Participant.objects.create(
        ip_address=ip_address, session=session, name=name
    )


def get_participant_by_id(participant_id):
    return models.Participant.objects.get(id=participant_id)


def get_participants_json(session: models.Session):
    return serialisers.ParticipantSerialiser(session.participant_set, many=True).data


def get_session(session_id: str):
    return models.Session.objects.get(pk=session_id)


def compare_session_user(session: models.Session, user):
    return session.owner == user


def init_text_message(session: models.Session, text):
    message = models.Message(session=session)
    message.save()
    t_message = models.TextMessage(message=message, content=text)
    t_message.save()

    return message


def open_session(session: models.Session):
    session.is_running = True
    session.save()


def close_session(session: models.Session):
    session.is_running = False
    session.save()


def authentication_check(user: User):
    return user.is_authenticated


def user_is_owner(session: models.Session, user: User):
    return user.is_authenticated and user == session.owner


def get_session_status(session: models.Session):
    return session.status


class SessionConsumer(AsyncWebsocketConsumer):
    async def broadcast_participants(self):
        participants_srl = await sync_to_async(get_participants_json)(self.session)

        await self.channel_layer.group_send(
            self.session_group_name,
            {
                "type": "send_participants",
                "message": participants_srl,
            },
        )

    async def send_participants(self, event):
        await self.send(
            text_data=json.dumps(
                {
                    "method": "list_participants",
                    "message": event["message"],
                }
            )
        )


class ControllerConsumer(SessionConsumer):
    async def connect(self):
        self.session_id = self.scope["url_route"]["kwargs"]["session_id"]
        self.session_group_name = f"session_{self.session_id}"

        self.user = self.scope["user"]
        auth_check = sync_to_async(authentication_check)(self.user)
        try:
            assert auth_check
        except AssertionError:
            await self.accept_warn_close(
                f"User {self.user.username} is not authenticated."
            )
            return

        try:
            self.session: models.Session = await ds2a(get_session)(self.session_id)
            self.user_is_owner: bool = await sync_to_async(compare_session_user)(
                self.session, self.user
            )
            assert self.user_is_owner  # and self.source = "controller"
        except AssertionError:
            await self.accept_warn_close(f"You are not the owner of this session.")
            return
        # except:
        #     TODO?
        #     await self.accept_warn_close(f"Session with this ID ({self.session_id}) not found!")
        #     return

        # Join session group
        await self.channel_layer.group_add(self.session_group_name, self.channel_name)

        await ds2a(open_session)(self.session)
        await self.accept()
        # await self.send_participants()

    async def disconnect(self, _):
        # Leave session group
        await self.channel_layer.group_discard(
            self.session_group_name, self.channel_name
        )
        await ds2a(close_session)(self.session)

    async def receive(self, text_data):
        data_json = json.loads(text_data)
        method = data_json["method"]

        if method == "set_status":
            await self.set_status(data_json.get("status"))
        elif method == "set_password":
            await ds2a(self.session.set_password)(data_json.get("password"))
        elif method == "set_participant_permission":
            await self.set_participant_permission(
                data_json["participant_id"], data_json["permitted"]
            )
        elif method == "new_text_message":
            message = data_json.get("message")
            message_obj = await ds2a(init_text_message)(self.session, message)
            message_srl = serialisers.MessageSerialiser(message_obj).data

            # Send message to session group
            await self.channel_layer.group_send(
                self.session_group_name,
                {
                    "type": data_json.get("method"),
                    "message": message_srl,
                },
            )
        else:
            LOGGER.warning(
                f"Method {method} attempted by user {self.user} with data {json.dumps(data_json)}"
            )

    async def accept_warn_close(self, message):
        await self.accept()
        await self.send_warning(message)
        await self.close()

    # async def remove_all_participants(self, _):
    #     if self.user_is_owner and self.connection_type == "controller":
    #         await ds2a(remove_all_participants)(self.session, self.participant)
    #         await self.broadcast_participants()

    async def new_text_message(self, event):
        message_srl = event["message"]

        await self.send(
            text_data=json.dumps(
                {
                    "method": "new_text_message",
                    "message": message_srl,
                }
            )
        )

    async def send_warning(self, message):
        await self.send(
            text_data=json.dumps(
                {
                    "method": "warning",
                    "message": message,
                }
            )
        )

    async def set_participant_permission(self, participant_id, permitted):
        LOGGER.debug(
            f"Participant {participant_id} permission being set to {permitted}"
        )
        participant: models.Participant = await ds2a(get_participant_by_id)(
            participant_id
        )
        await ds2a(participant.set_permission)(permitted)
        await self.broadcast_participants()

    async def set_status(self, status):
        try:
            assert status in [choice[0] for choice in models.Session.Status.choices]
            await ds2a(set_status)(self.session, status)
        except AssertionError:
            LOGGER.warning(
                f"User {self.user} tried to set session {self.session_id} status to {status}"
            )
            self.send_warning(f"{status} is not a status option.")


class ReceiverConsumer(SessionConsumer):
    async def connect(self):
        self.session_id = self.scope["url_route"]["kwargs"]["session_id"]
        self.session_group_name = f"session_{self.session_id}"

        try:
            self.session: models.Session = await ds2a(get_session)(self.session_id)
            assert self.session.is_running

        except AssertionError:
            await self.accept()
            await self.send(
                text_data=json.dumps(
                    {
                        "method": "warning",
                        "message": "This session is not open at the moment.",
                    }
                )
            )
            await self.close()
            return
        except:
            await self.accept()
            await self.send(
                text_data=json.dumps(
                    {
                        "method": "warning",
                        "message": f"Session with this ID ({self.session_id}) not found!",
                    }
                )
            )
            await self.close()
            return

        # session_status = await ds2a(get_status(self.session))(self.session)
        if await sync_to_async(user_is_owner)(self.session, self.scope["user"]):
            self.participant: models.Participant = await ds2a(make_participant)(
                self.scope["client"][0],
                self.session,
                f"{self.scope['user'].username} (you)",
            )
            LOGGER.debug(
                f"Participant {self.participant.id} owns session {self.session.id}"
            )
            await ds2a(self.participant.set_permission)(True)
            # await ds2a(set_participant_permission)(self.participant, True)
        else:
            self.participant: models.Participant = await ds2a(make_participant)(
                self.scope["client"][0], self.session
            )

            if self.session.status == models.Session.Status.OPEN_DOOR:
                LOGGER.debug(
                    f"Session status is set to {models.Session.Status.OPEN_DOOR}"
                )
                await ds2a(self.participant.set_permission)(True)

        # Join session group
        await self.channel_layer.group_add(self.session_group_name, self.channel_name)

        await self.accept()

        await self.broadcast_participants()

    async def disconnect(self, close_code):
        # Leave session group
        await self.channel_layer.group_discard(
            self.session_group_name, self.channel_name
        )

        try:
            await ds2a(self.participant.delete)()
        except AttributeError:
            pass

        await self.broadcast_participants()

    async def new_text_message(self, event):
        message_srl = event["message"]

        LOGGER.debug("Sending new text message")

        participant: models.Participant = await ds2a(get_participant_by_id)(
            self.participant.id
        )
        is_permitted = participant.is_permitted
        LOGGER.debug(f"{participant.id} 3 is permitted: {is_permitted}")

        if is_permitted:
            await self.send(
                text_data=json.dumps(
                    {
                        "method": "new_text_message",
                        "message": message_srl,
                    }
                )
            )
            await ds2a(participant.increment)()

    async def receive(self, text_data):
        data_json = json.loads(text_data)
        method = data_json["method"]

        if method == "submit_participant_details":
            await self.set_name(data_json.get("name"))
            await self.check_password(data_json.get("password"))

        else:
            LOGGER.warning(
                f"Method {method} attempted by user {self.user} with data {json.dumps(data_json)}"
            )

    async def set_name(self, name: str):
        if name is None:
            return

        await ds2a(self.participant.set_name)(name)
        await self.broadcast_participants()

    async def check_password(self, password: str):
        password_check = await ds2a(self.session.check_password)(password)
        await ds2a(self.participant.set_permission)(password_check)
        await self.broadcast_participants()
