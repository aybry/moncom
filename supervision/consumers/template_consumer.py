import json

from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from asgiref.sync import sync_to_async
from ..api import serialisers
from .. import models


from pprint import pprint


def init_predef_text_message(template, data):
    predef_message = models.PredefText(template=template, **data)
    predef_message.save()

    return serialisers.PredefTextSerialiser(predef_message).data


def fetch_all_predef_messages(template):
    predef_text_objs = models.PredefText.objects.filter(template=template)
    # predef_images = models.PredefImage.objects.all()
    return serialisers.PredefTextSerialiser(predef_text_objs, many=True).data


def get_template(template_id):
    return models.Template.objects.get(pk=template_id)


def compare_template_user(template, user):
    return template.owner == user


def authentication_check(user):
    return user.is_authenticated


class TemplateConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.template_id = self.scope["url_route"]["kwargs"]["template_id"]
        self.template_group_name = f"template_{self.template_id}"
        self.user = self.scope["user"]

        try:
            self.template = await database_sync_to_async(get_template)(self.template_id)
            self.user_is_owner = await sync_to_async(compare_template_user)(
                self.template, self.user
            )
            assert self.user_is_owner  # and self.source = "controller"
        except AssertionError:
            await self.accept_warn_close(f"You are not the owner of this template.")
            return

        auth_check = sync_to_async(authentication_check)(self.user)
        try:
            assert auth_check
        except AssertionError:
            await self.accept_warn_close(
                f"User {self.user.username} is not authenticated."
            )
            return

        await self.channel_layer.group_add(self.template_group_name, self.channel_name)

        await self.accept()

    async def disconnect(self, _):
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        pprint(text_data_json)
        method = text_data_json.pop("method")
        if method == "new_predef_message":
            await self.create_new_predef_message(text_data_json)
        elif method == "fetch_predef_messages":
            await self.fetch_predef_messages()

    async def create_new_predef_message(self, text_data_json):
        message_srl = await database_sync_to_async(init_predef_text_message)(
            self.template, text_data_json
        )
        print(message_srl)

        await self.channel_layer.group_send(
            self.template_group_name,
            {
                "type": "new_predef_text",
                "message": message_srl,
            },
        )

    async def fetch_predef_messages(self):
        all_predef_messages_srl = await database_sync_to_async(
            fetch_all_predef_messages
        )(self.template)

        await self.send(
            text_data=json.dumps(
                {
                    "method": "fetch_predef_messages",
                    "messages": all_predef_messages_srl,
                }
            )
        )

    async def new_predef_text(self, event):
        message_srl = event["message"]

        await self.send(
            text_data=json.dumps(
                {
                    "method": "new_predef_text",
                    "message": message_srl,
                }
            )
        )

    async def accept_warn_close(self, message):
        await self.accept()
        await self.send(
            text_data=json.dumps(
                {
                    "method": "warning",
                    "message": message,
                }
            )
        )
        await self.close()
