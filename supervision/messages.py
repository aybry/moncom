def get_message(message_name, kwargs):
    return eval(message_name).format(**kwargs).replace("\n", " ")


USER_ALREADY_REGISTERED = """
You're already registered.
"""


EMAIL_VERIFY_SENT = """
A verification email has been sent to {email}. Please check your inbox and
follow the instructions contained in the email.
"""


EMAIL_VERIFIED = """
Thanks for verifying your email address. Welcome to MonCom, {username}!
"""


ACCOUNT_ACTIVATION_LINK_INVALID = """
Account activation link is invalid.
"""


PASSWORD_RESET = """
You have requested a password reset link for the email address {email}. If this email
address is valid, an email has been sent containing a link to reset your password.
"""


ERROR = """
An error occured: {timestamp} / {error_message}.
"""
