from django.urls import path, include, reverse_lazy, reverse
from django.contrib.auth import views as auth_views

from .views import session_views
from .views import core_views
from .views import account_views

# from .api.urls import router


urlpatterns = [
    path("", core_views.HomeView.as_view(), name="home"),
    path("dashboard", core_views.DashboardView.as_view(), name="dashboard"),
    # path('accounts/', include('django.contrib.auth.urls')),
    path("accounts/register", account_views.SignupView.as_view(), name="register"),
    path(
        "accounts/activate/<uidb64>/<token>",
        account_views.ActivateView.as_view(),
        name="activate",
    ),
    path(
        "accounts/log_in",
        auth_views.LoginView.as_view(
            template_name="accounts/log_in.html",
            redirect_authenticated_user=True,
            redirect_field_name="home",
        ),
        name="log_in",
    ),
    path(
        "accounts/log_out",
        auth_views.LogoutView.as_view(next_page="home"),
        name="log_out",
    ),
    path(
        "accounts/password_change",
        account_views.PasswordChangeView.as_view(
            template_name="accounts/password_change.html",
            success_url=reverse_lazy("user"),
        ),
        name="password_change",
    ),
    # path('accounts/password_change/done',
    #      auth_views.PasswordChangeDoneView.as_view(next_page='user'),
    #      name='password_change_done'),
    path(
        "accounts/password_reset",
        account_views.PasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "accounts/reset/<uidb64>/<token>",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="accounts/password_reset_confirm.html"
        ),
        name="password_reset_confirm",
    ),
    path(
        "accounts/reset/done",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="accounts/password_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
    path("user", account_views.UserView.as_view(), name="user"),
    path("sessions", session_views.SessionsListView.as_view(), name="sessions"),
    path(
        "controller",
        session_views.QuickstartSessionView.as_view(),
        name="create_session",
    ),
    path(
        "controller/<session_id>",
        session_views.ControllerView.as_view(),
        name="controller",
    ),
    path(
        "receiver/<session_id>", session_views.ReceiverView.as_view(), name="receiver"
    ),
    path("templates", session_views.TemplatesListView.as_view(), name="templates"),
    path(
        "templates/<uuid:pk>",
        session_views.EditTemplateView.as_view(),
        name="edit_template",
    ),
]
