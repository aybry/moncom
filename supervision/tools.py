import uuid


def make_short_uuid():
    """
    Returns the first section of a uuid4. Obviously not even close to the uniqueness of a uuid4, but fulfills the following criteria:

    - sufficiently unique for sessions (16 ^ 12)
    - not guessable (as with increasing integers)
    - easy enough to type if necessary (12 characters)
    """
    return str(uuid.uuid4()).split("-")[-1].lower()
