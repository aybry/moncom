import socket
import logging

from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils.html import strip_tags
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_text
from django.contrib.auth.tokens import default_token_generator


SENDER_EMAIL_ADDRESS = "info@moncom.ay-bryson.com"
BCC_EMAIL_ADDRESS = "moncom@ay-bryson.com"

if socket.gethostname() in ["samuel-acer", "aybry-home"]:
    PROTOCOL = "http"
else:
    PROTOCOL = "https"


def render_templates(template_name, context):
    logging.info("Rendering templates")
    html_message = loader.render_to_string(f"emails/{template_name}.html", context)
    text_message = strip_tags(html_message)

    return html_message, text_message


def user_registered(user, site):
    logging.info(f"Preparing email for new user email activation ({user.username})")

    subject = "MonCom: Activate your account"

    context = {
        "username": user.username,
        "protocol": "http" if "localhost" in site.name else "https",
        "domain": site.domain,
        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
        "token": default_token_generator.make_token(user),
    }

    html_message, text_message = render_templates("signup_message", context)

    email = EmailMultiAlternatives(
        subject,
        text_message,
        SENDER_EMAIL_ADDRESS,
        [user.email],
    )

    logging.info("Email prepared")

    return email


def reset_password(user, site):
    logging.info(f"Preparing email for password reset (user {user.username})")

    subject = "MonCom: Reset your password"

    context = {
        "username": user.username,
        "protocol": "http" if "localhost" in site.name else "https",
        "domain": site.domain,
        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
        "token": default_token_generator.make_token(user),
    }

    html_message, text_message = render_templates("password_reset_message", context)

    email = EmailMultiAlternatives(
        subject,
        text_message,
        SENDER_EMAIL_ADDRESS,
        [user.email],
    )

    logging.info("Email prepared")

    return email
