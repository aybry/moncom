import re
import uuid
import logging

from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from django.db.models.fields import related
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

# from versatileimagefield.fields import VersatileImageField, PPOIField

from .tools import make_short_uuid


LOGGER = logging.getLogger("moncom")
User._meta.get_field("email")._unique = True


class Profile(models.Model):
    class Subscription(models.TextChoices):
        BASIC = "BSC", _("Basic")
        SUPPORTER = "SPT", _("Supporter")
        PROFESSIONAL = "PRO", _("Professional")

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    ts_and_cs_agreed = models.BooleanField(default=False)
    ts_and_cs = models.CharField(max_length=10000, blank=True)
    reset_password = models.BooleanField(default=False)
    subscription = models.CharField(
        max_length=3, choices=Subscription.choices, default=Subscription.BASIC
    )


class Participant(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    joined_at = models.DateTimeField(auto_now_add=True)
    last_seen = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=80, default="")
    ip_address = models.CharField(max_length=15, default="")
    is_permitted = models.BooleanField(default=False)
    messages_received = models.IntegerField(default=0)
    session = models.ForeignKey("Session", on_delete=models.CASCADE, null=True)

    def set_permission(self, permitted):
        self.is_permitted = permitted
        self.save()
        LOGGER.debug(f"Set participant {self.id} permission to {permitted}")

    def increment(self):
        self.messages_received += 1
        self.save()

    def set_name(self, name):
        self.name = name
        self.save()
        LOGGER.debug(f"Set participant {self.id} name to {name}")

    class Meta:
        ordering = ["joined_at"]


class Session(models.Model):
    class Status(models.TextChoices):
        OPEN_DOOR = "OPN", _("Open door")
        CLOSED_DOOR = "CLS", _("Closed door")
        PASSWORD_PROTECTED = "PWD", _("Password protected")

    id = models.CharField(
        primary_key=True,
        max_length=12,
        default=make_short_uuid,
        unique=True,
        editable=False,
    )
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    template = models.ForeignKey(
        "Template", on_delete=models.CASCADE, null=True, blank=True
    )
    name = models.CharField(max_length=64, default="Quickstart", null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    status = models.CharField(
        max_length=3, choices=Status.choices, default=Status.OPEN_DOOR
    )
    last_used = models.DateTimeField(auto_now=True, null=True)
    current_message = models.OneToOneField(
        "Message",
        on_delete=models.CASCADE,
        null=True,
        related_name="currently_in_session",
    )
    is_running = models.BooleanField(default=False)
    latest_run_start = models.DateTimeField(auto_now=True, null=True, blank=True)
    password = models.CharField(max_length=256, null=True, blank=True)

    class Meta:
        ordering = ["-last_used"]

    def set_password(self, password):
        self.password = password
        self.save()
        LOGGER.debug(f"Set session {self.id} password to {password}")

    def check_password(self, password):
        return password == self.password


class Message(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(blank=True, default=now)
    from_template = models.BooleanField(default=False)

    class Meta:
        ordering = ["-timestamp"]

    def get_type(self):
        if hasattr(self, "text"):
            return "text"
        elif hasattr(self, "image"):
            return "image"

    def get_child(self):
        if self.get_type() == "text":
            return self.text
        elif self.get_type() == "image":
            return self.image
        else:
            return ""


class Template(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=64, default="New template")
    last_used = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return f"{self.name} ({len(self.text_set.all())} texts, {len(self.image_set.all())} images)"


class PredefText(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    template = models.ForeignKey(
        Template, on_delete=models.CASCADE, null=True, related_name="text_set"
    )
    content = models.CharField(max_length=1000, editable=False)
    duration = models.DecimalField(decimal_places=1, max_digits=8, default=0)

    class Meta:
        verbose_name_plural = "Predefined texts"

    def strip_html(self):
        """Removes html code from a string so that word counts are accurate.

        e.g. if self.content = "<p style="text-align: center;">Example message</p>",
            the function returns "Example message"

        Returns:
            str: `self.content` with html tags removed.
        """
        clean_regex = re.compile("<.*?>")
        return re.sub(clean_regex, "", self.content)


class PredefImage(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    template = models.ForeignKey(
        Template, on_delete=models.CASCADE, null=True, related_name="image_set"
    )
    content = models.ImageField(upload_to="session_images")
    duration = models.DecimalField(decimal_places=1, max_digits=8, default=0)

    class Meta:
        verbose_name_plural = "Predefined images"


class TextMessage(models.Model):
    message = models.OneToOneField(
        Message, on_delete=models.CASCADE, related_name="text"
    )
    content = models.CharField(max_length=1000, editable=False)
    duration = models.DecimalField(decimal_places=1, max_digits=8, default=0)

    class Meta:
        verbose_name_plural = "Text messages"

    def strip_html(self):
        """Removes html code from a string so that word counts are accurate.

        e.g. if self.content = "<p style="text-align: center;">Example message</p>",
            the function returns "Example message"

        Returns:
            str: `self.content` with html tags removed.
        """
        clean_regex = re.compile("<.*?>")
        return re.sub(clean_regex, "", self.content)


class ImageMessage(models.Model):
    message = models.OneToOneField(
        Message, on_delete=models.CASCADE, related_name="image"
    )
    content = models.ForeignKey(PredefImage, on_delete=models.CASCADE)
    duration = models.DecimalField(decimal_places=1, max_digits=8, default=0)

    class Meta:
        verbose_name_plural = "Image messages"


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
