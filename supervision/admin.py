from django.contrib import admin

from . import models


@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "email_confirmed",
        "ts_and_cs_agreed",
        "ts_and_cs",
        # 'reset_password',
        "subscription",
    )


@admin.register(models.Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = (
        "owner",
        "name",
        "status",
        "is_running",
        "get_messages",
    )

    def get_messages(self, obj):
        return obj.message_set.all()


@admin.register(models.Template)
class TemplateAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = (
        "session",
        "timestamp",
    )


# @admin.register(models.PredefText)
# class PredefTextAdmin(admin.ModelAdmin):
#     list_display = (
#         'template',
#         'content',
#     )


# @admin.register(models.PredefImage)
# class PredefImageAdmin(admin.ModelAdmin):
#     list_display = (
#         'name',
#         "duration",
#         "image_ppoi",
#         'template',
#         'content',
#     )


@admin.register(models.TextMessage)
class TextMessageAdmin(admin.ModelAdmin):
    list_display = (
        "message",
        "content",
        "duration",
    )


@admin.register(models.ImageMessage)
class ImageMessageAdmin(admin.ModelAdmin):
    pass
    # list_display = (
    #     'message',
    #     'content',
    #     'duration',
    # )


@admin.register(models.Participant)
class ParticipantAdmin(admin.ModelAdmin):
    pass
