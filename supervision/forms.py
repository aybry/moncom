from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from . import models


class SignUpForm(UserCreationForm):
    email = forms.EmailField(
        max_length=254, help_text="Required. Please use a valid email address."
    )
    # ts_and_cs_agreed = forms.BooleanField(required=True,
    #                                       help_text='I have read and agree to the terms and conditions.',
    #                                       label='I agree to the terms and conditions')

    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "password1",
            "password2",
            # 'ts_and_cs_agreed',
        )


class SessionForm(forms.ModelForm):
    class Meta:
        model = models.Session

        fields = ("name",)
        labels = {
            "name": "Session name",
        }


class SelectSessionTemplateForm(forms.ModelForm):
    def __init__(self, current_user, *args, **kwargs):
        super(SelectSessionTemplateForm, self).__init__(*args, **kwargs)
        self.fields["template"].queryset = self.fields["template"].queryset.filter(
            owner=current_user
        )

    class Meta:
        model = models.Session

        fields = ("template",)
        labels = {
            "template": "Select template",
        }


class TemplateForm(forms.ModelForm):
    class Meta:
        model = models.Template

        fields = ("name",)
        labels = {
            "name": "Template name",
        }


# class PredefTextForm(forms.ModelForm):
#     class Meta:
#         model = models.PredefText

#         fields = (
#             'content',
#             'duration',
#         )
#         labels = {
#             'duration': 'Display for (seconds)'
#         }


# class PredefImageForm(forms.ModelForm):
#     class Meta:
#         model = models.PredefImage

#         fields = (
#             'content',
#             'duration',
#         )
#         labels = {
#             'duration': 'Display for (seconds)'
#         }
