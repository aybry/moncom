from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.urls import reverse

from django.views import View
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from django.views.decorators.clickjacking import xframe_options_sameorigin

from .. import models, forms


class HomeView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect(reverse("dashboard"))
        else:
            return redirect(reverse("log_in"))


@method_decorator(login_required, name="dispatch")
class DashboardView(TemplateView):
    template_name = "supervision/dashboard.html"
