from datetime import datetime
import requests
import logging
import urllib
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.models import User
from django.views.generic.base import TemplateView
from django.views import View
from django.utils.encoding import force_text
from django.contrib.auth import login
from django.contrib.auth.forms import PasswordResetForm, PasswordChangeForm
from django.utils.http import urlsafe_base64_decode
from django.urls import reverse
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth import views as auth_views
from django.conf import settings

from .. import models, forms, emails, tokens
from ..messages import get_message


@method_decorator(login_required, name="dispatch")
class UserView(TemplateView):
    template_name = "supervision/user.html"

    # def get(self, request):
    #     if request.user.is_authenticated:
    #         return redirect(reverse("dashboard"))


class SignupView(FormView):
    template_name = "accounts/sign_up.html"
    form_class = forms.SignUpForm
    success_url = "/"

    def get(self, request):
        if request.user.is_authenticated:
            message = get_message("USER_ALREADY_REGISTERED", {})
            messages.add_message(self.request, messages.INFO, message)
            return redirect(reverse("dashboard"))
        else:
            return super().get(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["recaptcha_site_key"] = settings.RECAPTCHA_PUBLIC_KEY
        return context

    def form_valid(self, form):
        recaptcha_response = self.request.POST.get("g-recaptcha-response")
        url = "https://www.google.com/recaptcha/api/siteverify"
        data = {
            "secret": settings.RECAPTCHA_PRIVATE_KEY,
            "response": recaptcha_response,
        }
        r = requests.post(url, data=data)
        result = r.json()

        if result["success"]:
            logging.info("Creating new user")
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            site = get_current_site(self.request)

            email = emails.user_registered(user, site)

            try:
                email.send()
                logging.info("Email sent")
                message = get_message("EMAIL_VERIFY_SENT", {"email": user.email})
                messages.add_message(self.request, messages.SUCCESS, message)
            except Exception as error:
                logging.error(error)

                message = get_message(
                    "ERROR",
                    {"timestamp": datetime.now().isoformat(), "error_message": error},
                )
                messages.add_message(self.request, messages.WARNING, message)
        else:
            messages.add_message(
                self.request,
                messages.WARNING,
                "reCAPTCHA was invalid. Please try again.",
            )

        return super().form_valid(form)


class ActivateView(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist) as error:
            message = get_message(
                "ERROR",
                {"timestamp": datetime.now().isoformat(), "error_message": error},
            )
            messages.add_message(request, messages.WARNING, message)
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            user.is_active = True
            user.profile.email_confirmed = True
            user.save()
            login(request, user)

            message = get_message("EMAIL_VERIFIED", {"username": user.username})
            messages.add_message(request, messages.INFO, message)
            return redirect("dashboard")
        else:
            message = get_message("ACCOUNT_ACTIVATION_LINK_INVALID", {})
            messages.add_message(request, messages.WARNING, message)
            return redirect("home")


class PasswordResetView(FormView):
    template_name = "accounts/password_reset.html"
    form_class = PasswordResetForm

    def form_valid(self, form):
        email_address = form["email"].value()
        logging.info(f"Password reset link requested for email {email_address}")

        try:
            user = User.objects.get(email=email_address)
            logging.info(f"User {user.username} found")
            site = get_current_site(self.request)
            email = emails.reset_password(user, site)
            email.send()
            logging.info("Email sent")
        except User.DoesNotExist:
            logging.info(f"No user found with that email address")
        except Exception as error:
            logging.error(error)

        message = get_message("PASSWORD_RESET", {"email": email_address})
        messages.add_message(self.request, messages.INFO, message)

        return redirect(reverse("home"))


class PasswordChangeView(auth_views.PasswordChangeView):
    def form_valid(self, form):
        messages.add_message(self.request, messages.INFO, "Your password was changed")

        return super().form_valid(form)
