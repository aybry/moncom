from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.urls import reverse

from django.views import View
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from django.views.decorators.clickjacking import xframe_options_sameorigin

from .. import models, forms


@method_decorator(login_required, name="dispatch")
class ControllerView(TemplateView):
    template_name = "supervision/controller/main.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # try:
        session = models.Session.objects.get(pk=kwargs.get("session_id"))
        context.update({"session": session})
        # except models.Session.DoesNotExist:
        #     context.update({'message': 'Session not found! Check URL.'})

        context.update(
            {
                "show_controller_view": True,
                "select_template_form": forms.SelectSessionTemplateForm(
                    current_user=self.request.user, instance=session
                ),
            }
        )

        return context

    def post(self, request, session_id):
        form_label = request.POST.get("form_label")
        if form_label == "select_template":
            session = models.Session.objects.get(pk=session_id)
            template = models.Template.objects.get(pk=request.POST.get("template"))
            session.template = template
            session.save()

        return redirect(reverse("controller", kwargs={"session_id": session.id}))


@method_decorator(login_required, name="dispatch")
class SessionsListView(TemplateView):
    template_name = "supervision/sessions.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sessions"] = models.Session.objects.filter(owner=self.request.user)
        context["session_form"] = forms.SessionForm
        return context

    def post(self, request):
        session = models.Session()
        session.owner = request.user
        session.name = request.POST.get("name")
        session.save()

        return redirect(reverse("controller", kwargs={"session_id": session.id}))


@method_decorator(login_required, name="dispatch")
class QuickstartSessionView(View):
    def get(self, request):
        session = models.Session()
        session.owner = self.request.user
        session.save()

        return redirect(reverse("controller", kwargs={"session_id": session.id}))


@method_decorator(xframe_options_sameorigin, name="get")
class ReceiverView(TemplateView):
    template_name = "supervision/receiver.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        try:
            session = models.Session.objects.get(pk=kwargs.get("session_id"))
            context.update({"session": session})
        except models.Session.DoesNotExist:
            context.update({"message": "Session not found! Check URL."})
        return context


@method_decorator(login_required, name="dispatch")
class TemplatesListView(TemplateView):
    template_name = "supervision/templates.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["templates"] = models.Template.objects.filter(owner=self.request.user)
        context["template_form"] = forms.TemplateForm
        return context

    def post(self, request):
        template_set = models.Template()
        template_set.owner = request.user
        template_set.name = request.POST.get("name")
        template_set.save()

        return redirect(reverse("edit_template", kwargs={"pk": template_set.id}))


@method_decorator(login_required, name="dispatch")
class EditTemplateView(DetailView):
    template_name = "supervision/template_editor.html"
    model = models.Template

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["show_template_view"] = True

        return context
